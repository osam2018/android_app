package com.example.user.testapplication;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class CreateActivity extends AppCompatActivity {

    private int num = 0;
    private int type = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        Intent intent = getIntent();
        num = intent.getIntExtra("num", 0);
        type = intent.getIntExtra("type", -1);
        if(num < 0 || type < 0)
            finish();

        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();
                intent.putExtra("type", type);
                intent.putExtra("num", num);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_update_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            Intent intent = getIntent();
            setResult(RESULT_CANCELED, intent);
            finish();
            return true;
        } else if (id == R.id.check_Button) {
            //db에 새로운 내용으로 저장
            Intent intent = getIntent();
            intent.putExtra("type", type);
            intent.putExtra("num", num);
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    */
}
