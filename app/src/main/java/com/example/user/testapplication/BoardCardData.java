package com.example.user.testapplication;

public class BoardCardData {
    public String title;
    public String nickName;
    public String date;
    public String contents;
    public BoardCardData(String title, String nickName, String date, String contents){
        this.title = title;
        this.nickName = nickName;
        this.date = date;
        this.contents = contents;
    }
}
