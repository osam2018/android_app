package com.example.user.testapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class ReadActivity extends AppCompatActivity {

    private int num = 0;
    private int type = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        Intent intent = getIntent();
        num = intent.getIntExtra("num", 0);
        type = intent.getIntExtra("type", -1);
        if(num < 0 || type < 0)
            finish();
        load();
    }

    private void load() {
        //DB에서 해당 인덱스값 제목, 내용 받아옴.
    }

    /*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.read_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.update_Button) {
            Intent intent = new Intent(this, UpdateActivity.class);
            intent.putExtra("num", num);
            intent.putExtra("type", type);
            startActivityForResult(intent, 11);
            return true;
        } else if (id == R.id.delete_Button) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.delete);
            builder.setIcon(R.drawable.delete_check_icon);
            builder.setMessage(R.string.delete_check);
            builder.setNegativeButton(R.string.no, null);
            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //db에서 삭제
                    Snackbar.make(findViewById(R.id.read_layout), R.string.delete_message, Snackbar.LENGTH_LONG).show();
                    finish();
                }
            });
            builder.setCancelable(false);
            AlertDialog deleteDialog = builder.create();
            deleteDialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == 11)
            if(resultCode == RESULT_OK) {
                Snackbar.make(findViewById(R.id.read_layout), R.string.update_message, Snackbar.LENGTH_LONG).show();
                load();
            }else if(resultCode == RESULT_CANCELED) {
                Snackbar.make(findViewById(R.id.read_layout), R.string.cancel_message, Snackbar.LENGTH_LONG).show();
            }
    }

}
