package com.example.user.testapplication;



import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class BoardAdapter extends RecyclerView.Adapter<BoardAdapter.BoardViewHolder> {

    private List<BoardCardData> list;
    public BoardAdapter(List<BoardCardData> list){
        this.list = list;
    }

    @Override
    public BoardViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.board_card, parent, false);
        return new BoardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BoardViewHolder holder, int pos){
        holder.title.setText(list.get(pos).title);
        holder.nickName.setText(list.get(pos).nickName);
        holder.date.setText(list.get(pos).date);
        holder.contents.setText(list.get(pos).contents);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class BoardViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView nickName;
        TextView date;
        TextView contents;

        public BoardViewHolder(View view){
            super(view);
            title = view.findViewById(R.id.board_card_title);
            nickName = view.findViewById(R.id.board_card_nickname);
            date = view.findViewById(R.id.board_card_date);
            contents = view.findViewById(R.id.board_card_contents);
        }
    }


}
