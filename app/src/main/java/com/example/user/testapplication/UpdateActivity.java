package com.example.user.testapplication;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class UpdateActivity extends AppCompatActivity {

    private TextView title;
    private TextView contents;
    private int type = -1;
    private int num = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        ActionBar bar = getSupportActionBar();
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.update);

        title = findViewById(R.id.title_update);
        contents = findViewById(R.id.contents_update);

        Intent intent = getIntent();
        num = intent.getIntExtra("num", 0);
        type = intent.getIntExtra("type", -1);
        if(num < 0 || type < 0)
            finish();
        //DB에서 해당 인덱스값 제목, 내용 받아옴.
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_update_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            setResult(RESULT_CANCELED, getIntent());
            finish();
            return true;
        } else if (id == R.id.check_Button) {
            //db에 기존 내용을 새로운 내용으로 저장
            Intent intent = getIntent();
            intent.putExtra("type", type);
            intent.putExtra("num", num);
            setResult(RESULT_OK, intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
