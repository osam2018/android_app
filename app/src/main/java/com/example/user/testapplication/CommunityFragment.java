package com.example.user.testapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class CommunityFragment extends Fragment {

    public BottomNavigationView navigationView;
    public RecyclerView.LayoutManager layoutManager;
    public RecyclerView recyclerView;
    public BoardAdapter boardAdapter;
    public List<BoardCardData> boardCardData;
    private int lastPosition = Board.FREE;
    private int cardIndex = 0; // 사용 중

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community, container, false);
        recyclerView = view.findViewById(R.id.board_container);
        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        boardCardData = new ArrayList<>();
        boardAdapter = new BoardAdapter(boardCardData);
        FloatingActionButton fab = view.findViewById(R.id.fab);

        recyclerView.setAdapter(boardAdapter);
        load(lastPosition);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) {
                    CommunityFragment.this.setNavigationVisibility(View.GONE);
                }else if (dy < 5){
                    CommunityFragment.this.setNavigationVisibility(View.VISIBLE);
                }

                if (!recyclerView.canScrollVertically(1)) {
                    loadOld(lastPosition); // 최하단에 스크롤할 경우 자동으로 추가 게시판 데이터 불러옴.
                }
            }


        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CreateActivity.class);
                intent.putExtra("type", lastPosition);
                startActivityForResult(intent, 10);
            }
        });

        navigationView = view.findViewById(R.id.bottom_navigation);
        navigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        int id = menuItem.getItemId();
                        if(id == R.id.menu_free) {
                            lastPosition = Board.FREE;
                        }else if (id == R.id.menu_sport) {
                            lastPosition = Board.SPORT;
                        }else if (id == R.id.menu_hobby) {
                            lastPosition = Board.HOBBY;
                        }else
                            return false;
                        load(lastPosition);
                        return true;
                    }
                }
        );
        navigationView.setOnNavigationItemReselectedListener(
                new BottomNavigationView.OnNavigationItemReselectedListener() {
                    @Override
                    public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
                        load(lastPosition);
                    }
                }
        );
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void setNavigationVisibility(int visibility){
        if(visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE)
            navigationView.setVisibility(visibility);
    }

    public void load(int pos) {
        boardCardData.clear();
        //데이터베이스에서 가져옴 // 20개 가져옴 끝에 다다르면 loadOld로 추가로 로드
        cardIndex = 20; // 실제 DB인덱스 값으로 적용
        boardCardData.add(new BoardCardData(pos+"번", "lee", "04:02", "내용"));
        boardCardData.add(new BoardCardData(pos+"번", "lee", "04:02", "내용"));
        boardCardData.add(new BoardCardData(pos+"번", "lee", "04:02", "내용"));
        boardCardData.add(new BoardCardData(pos+"번", "lee", "04:02", "내용"));

        boardAdapter.notifyDataSetChanged();
        // 화면 갱신
    }

    public void loadOld(int pos) {
        // DB에서 인덱스 값 기준으로 가져옴
        cardIndex += 20;
        boardCardData.add(new BoardCardData(pos+"?", "crew", "08:20", "내용dlfksl"));
        boardCardData.add(new BoardCardData(pos+"?", "crew", "08:20", "내용dlfksl"));
        boardCardData.add(new BoardCardData(pos+"?", "crew", "08:20", "내용dlfksl"));
        boardCardData.add(new BoardCardData(pos+"?", "crew", "08:20", "내용dlfksl"));
        // 주의 사항 num 체크하여 조절해야
        boardAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == 10)
            if(resultCode == RESULT_OK) {
                final int num = data.getIntExtra("num", 0);
                final int type = data.getIntExtra("type", -1);
                Snackbar.make(getView().findViewById(R.id.board_container), R.string.add_message, Snackbar.LENGTH_LONG)
                        .setAction(R.string.see, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //해당 게시글로 이동
                                Intent intent = new Intent(getActivity(), ReadActivity.class);
                                intent.putExtra("num", num);
                                intent.putExtra("type", type);
                                startActivity(intent);
                            }
                        }).show();
            }else if(resultCode == RESULT_CANCELED) {
                Snackbar.make(getView().findViewById(R.id.board_container), R.string.cancel_message, Snackbar.LENGTH_LONG).show();
            }
    }
}
